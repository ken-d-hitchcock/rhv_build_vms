# Readme for rhv_build_vms role
Below are the variables and notes required to use te rhv_build_vms role.

## Base Variables
```
rhv_create_vms_username: <username@internal>
rhv_create_vms_password: <rhev password>
rhv_create_vms_hostname: <rhevm.fqdn>
rhv_create_vms_insecure: <true or false>

vmlist:
  vm:
    vmname:
    vmtype: "emptyvm or rhel7template"
    template: <name of template of use "na">
    cluster: "<cluster to use>"
    vmnic:
      - nic:
          vmnicstate: present
          nic_boot_protocol: <static or dynamic>
          nic_ip_address: <ip address>
          nic_netmask: <netmask>
          nic_gateway: <gateway>
          nic_on_boot: true
          vmnicname: <nic name>
          vmnicinterface: <interface type, use virtio>
          vmnicprofile: "<profile name>"
          vmnicnetwork: "<network name>"
    user_name: root
    state: <present or absent>
    vmmem: <size in GiB>
    vmcores: <number of cores>
    vmsockets: <number of sockets>
    vmdisks:
      - disk:
          vmdiskname: <disk name>
          vmdisksize: <size in GiB>
          vmdiskformat: <raw or cow> *not idempotent
          vmdiskinterface: <virtio should be used>
          vmdisksd: <storage domain to use>
          vmdiskstate: present
```