# Role name : rhv_create_vms
# Role Author : Ken Hitchcock


# Role to create VM in RHV based on the following variables.
# Credentials to the RHEV environment
- rhv_create_vms.create-vms.username:
rhv_create_vms.password:
rhv_create_vms.url:
rhv_create_vms.hostname:
rhv_create_vms.insecure:

#Dict used for vms to be created. The following variables are required.
vmlist:
  vmname:
    vmtype: <RHEL7 or emptyvm only accepted>
    template: <template name>
    cluster: <cluster name where vm should be deployed>
    nic_boot_protocol: <Protocol used when booting on the nic>
    nic_ip_address: <IP Address to assign>
    nic_netmask: <Netmask>
    nic_gateway: <Gateway>
    vmnic: <Name of NIC>
    nic_on_boot: <true or false>
    user_name: <user name on the vm>
    state: <present or absent>
    vmmem: <value with GiB>
    vmcores: <Number of CPU cores>
    vmsockets: <Number of CPU Sockets>
    vmdisk: <name of disk>
    vmdisksize: <size of disk value plus GiB>
    vmnic: <name of nic>
  vmname2:
    vmtype: emptyvm
    template: na
    cluster: devcluster
    nic_boot_protocol: static
    nic_ip_address: <IP Address to assign>
    nic_netmask: <Netmask>
    nic_gateway: <Gateway>
    vmnic: <Name of NIC>
    nic_on_boot: <true or false>
    user_name: <user name on the vm>
    state: <present or absent>
    vmmem: <value with GiB>
    vmcores: <Number of CPU cores>
    vmsockets: <Number of CPU Sockets>
    vmdisk: <name of disk>
    vmdisksize: <size of disk value plus GiB>
    vmnic: <name of nic>
